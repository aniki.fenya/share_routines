
const EUCL=function(a,b){
                    let s=a.map((e,i)=>{return  Math.pow(b[i]-e,2)})
                            .reduce((p,c)=>{return p+c});//)
                    return Math.sqrt(s);                   
                };
const ZA=(s)=>{return (new Array(s)).fill(0)}
const RND_A=function(s,range){
    return (new Array(s)).fill(0)
    .map(e=>{return Math.random()*(range[1]-range[0])-range[0]})};
const RMIN=0;
const RMAX=1;
var dSet={
    lRate:0.1,
    downPower:0.2,
    maxSteps:100,
    qSize:20,
    vLen:15,
    distanceFun:EUCL
}

var oEnt=(o)=>{return Object.entries(o)}

class KM{
    constructor(settings){
            var _self=this;
            oEnt(settings).map(entry=>{_self[entry[0]]=entry[1]});

            this.nodes_map=
                ZA(_self.qSize)
                    .map(el=>ZA(_self.qSize)
                        .map(e=>RND_A(_self.vLen,[RMIN,RMAX])))
    }

    findBMU(vec){
        var _self=this;
        var BMU={i:-1,j:-1,d:-1};
        this.nodes_map.map((row,i_ss)=>{
                row.map((cell,j_ss)=>{
                    let curCelDist=_self.distanceFun(cell,vec);
                    if(BMU.d<0){
                        BMU.d=curCelDist;BMU.i=i_ss;BMU.j=j_ss;
                    } 
                    else {BMU=BMU.d>curCelDist?
                            {i:i_ss,j:j_ss,d:curCelDist}:
                            BMU;}
                })})
            return BMU
        
    }

    pullNode(vec,i,j,rate){
        var cell=this.nodes_map[i][j];
        cell.map((e,i)=>{
            let step=((vec[i]-e)*rate);
            return cell[i]=e+step;
        }) 
    }

    expand(vec,BMU){
        var _self=this;
        divideNeighbors(BMU).slice(1).map((group,multiplyer)=>{
            group.map(pos=>{_self.pullNode(vec,pos[0],pos[1],_self.lRate/(multiplyer*_self.downPower))})
        })
    }

    train(data){
        var _self=this;
        while(this.maxSteps--){
            data.map((d)=>{
                let bmu=_self.findBMU(d);
                _self.pullNode(d,bmu.i,bmu.j,_self.lRate);
                _self.expand(d,bmu)
            })
        }
    }
    
}

var km=new KM(dSet);
var tst_dist=km.findBMU(RND_A(km.vLen,[RMIN,RMAX]));
var tst_set=
    [
     ...ZA(100).map(e=>RND_A(km.vLen,[-3,1])),
     ...ZA(100).map(e=>RND_A(km.vLen,[-1,3])),
     ...ZA(100).map(e=>RND_A(km.vLen,[2.5,6]))
    ];

function tryToPullHard(vec=tst_set[0],steps=100){
    while(steps--){
        km.pullNode(vec,0,0,0.01);
        console.log(km.distanceFun(vec,km.nodes_map[0][0]))
    }
}

function divideNeighbors(BMU){
        var gset={};
        km.nodes_map.map((row,i_ss)=>{
            row.map((cell,j_ss)=>{
                let dist2D=Math.sqrt(
                    Math.pow((i_ss-BMU.i),2)+
                    Math.pow((j_ss-BMU.j),2)
                ).toFixed(0);
                gset[dist2D]=gset[dist2D]?
                            gset[dist2D].concat([[i_ss,j_ss]]):
                            [[i_ss,j_ss]]
            })
        })
        return gset;
}

