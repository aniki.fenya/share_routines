from hashlib import sha256, sha1
import base64
from os import urandom
import traceback, sys
import bcolors


##########ORIGINAL###################################
#####################################################
# def make_link(g, file_ids):
#     """takes flask.g object and list _id of files/folders that will be available at this link. return a unique link"""
#     col = get_links_col(g)
#     link = base64.b64encode(sha1(urandom(64)).digest()).decode('utf-8').replace('/', 's')
#     try:
#         col.insert_one({'_id': link, 'files': list(map(obj_id, file_ids)), 'deleted': False}).inserted_id
#     except DuplicateKeyError:
#         link = make_link(g, file_ids)
#     return link

############ROUTINE BY chengyu2333##########################################
# https://github.com/chengyu2333/CMSpider/blob/master/cmspider/my_store.py
######################################################
# def store_file_list(self, data):
#     data = json.loads(data)
#
#     # infoResult.do
#     total_page = data['listInfo']['totalPages']
#     config['basic']['total_page'] = total_page
#     for item in data['listInfo']['content']:
#         file_url = "http://www.neeq.com.cn" + item['destFilePath']
#         title = item['disclosureTitle']
#         timestamp = item['upDate']['time']
#         # ??????(??)
#         # if config['basic']['max_replicate']:
#         #     if timestamp <= self.url_manager.get_last_url("file")['timestamp']:
#         #         raise exception.ListFinishedException
#         try:
#             self.url_manager.put_url(file_url, title, timestamp, "file")
#             # ???????????????0
#             Util.COUNT_DUPLICATE = 0
#             Util.COUNT_SUCCESS += 1
#         except errors.DuplicateKeyError as dk:
#             # ??????+1
#             Util.COUNT_DUPLICATE += 1
#             if config['list']['max_replicate']:
#                 if Util.COUNT_DUPLICATE > config['list']['max_replicate']:
#                     Util.COUNT_DUPLICATE = 0
#                     raise exception.ExceedMaxDuplicate
#                     # ?????????????????????
#     Util.view_bar(Util.COUNT_PROCESSED, total_page)
#     Util.COUNT_PROCESSED += 1
################################################


################# MY VISION ###############################
# return passed argument or generate exception 
# generation is independent from input exceptions raised randomly with prob ~ 0.85
# 
def schrodinger_box(link):
    if int.from_bytes(urandom(1), byteorder='big') < 220:
        raise BaseException(bcolors.WARN + "\ncat is dead" + bcolors.END)
    else:
        return link


def make_ulink(g=None, file_ids=None):
    # set counter of uniqueness exceptions
    retracts = 0
    # set MAX number of attempts to make link
    MAXRETRACTS = 4
    # set flag to indicate write made successfully
    success_writen = False
    # while success_writen is not true, which occurs if schrodinger_box do not rise exception we try to make unique link
    while not success_writen:
        # set new random link val
        link = base64.b64encode(sha1(urandom(64)).digest()).decode('utf-8').replace('/', 's')
        try:
            # try to use it somehow (write to mongo in original particular case)
            schrodinger_box(link)
            # if no exception rised set success_writen flag to true and break while loop
            success_writen = True
            break
        except BaseException:
            # in case exception occurs, incrementing counter of attempts
            retracts += 1
            print(traceback.format_exc())
            print(bcolors.WARN + "\nretracting" + bcolors.END)
        # after try/catch block check that retracts is not overflow MAXRETRACTS constant value if counter overflowed
        # break while loop
        if retracts >= MAXRETRACTS: break
    # after while loop ends, check success_writen is true (so we successfully pass schrodinger_box in <=MAXRETRACTS
    # attempts) if so return link
    if success_writen:
        return link
    # if not success_writen (means schrodinger_box raise exceptions on every call and counter was overflowed) print
    # message and raise another exception which should be handled by caller
    raise AttributeError(bcolors.WARN + "\nerror on link generation, sharing failed" + bcolors.END)


# to call from cli like "python unique_link_generation.py" and view result call our make_ulink function
try:
    print(bcolors.HEADER + "\n OMG we got it!!!\n" + make_ulink() + bcolors.END, file=sys.stdout)
    sys.exit(0)
# if link generated stdout message then exit with 0 error code
except AttributeError:
    print(traceback.format_exc(), file=sys.stderr)
    sys.exit(1)
# if link generation fails after MAXRETRACTS attempts stderror message then exit with non zero error code
